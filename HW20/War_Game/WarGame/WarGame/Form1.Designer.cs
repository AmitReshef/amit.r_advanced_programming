﻿namespace WarGame
{
    partial class GameScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.playerScoreLabel = new System.Windows.Forms.Label();
            this.forfeitButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.oppScoreLabel = new System.Windows.Forms.Label();
            this.opponentCard = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.opponentCard)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Your Score:";
            // 
            // playerScoreLabel
            // 
            this.playerScoreLabel.AutoSize = true;
            this.playerScoreLabel.Location = new System.Drawing.Point(168, 20);
            this.playerScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.playerScoreLabel.Name = "playerScoreLabel";
            this.playerScoreLabel.Size = new System.Drawing.Size(18, 20);
            this.playerScoreLabel.TabIndex = 1;
            this.playerScoreLabel.Text = "0";
            // 
            // forfeitButton
            // 
            this.forfeitButton.Location = new System.Drawing.Point(696, 14);
            this.forfeitButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.forfeitButton.Name = "forfeitButton";
            this.forfeitButton.Size = new System.Drawing.Size(158, 35);
            this.forfeitButton.TabIndex = 2;
            this.forfeitButton.Text = "Forfeit";
            this.forfeitButton.UseVisualStyleBackColor = true;
            this.forfeitButton.Click += new System.EventHandler(this.forfeitButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1329, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Opponent\'s Score:";
            // 
            // oppScoreLabel
            // 
            this.oppScoreLabel.AutoSize = true;
            this.oppScoreLabel.Location = new System.Drawing.Point(1468, 20);
            this.oppScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.oppScoreLabel.Name = "oppScoreLabel";
            this.oppScoreLabel.Size = new System.Drawing.Size(18, 20);
            this.oppScoreLabel.TabIndex = 4;
            this.oppScoreLabel.Text = "0";
            // 
            // opponentCard
            // 
            this.opponentCard.BackgroundImage = global::WarGame.Properties.Resources.card_back_blue;
            this.opponentCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.opponentCard.Location = new System.Drawing.Point(706, 88);
            this.opponentCard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.opponentCard.Name = "opponentCard";
            this.opponentCard.Size = new System.Drawing.Size(135, 182);
            this.opponentCard.TabIndex = 5;
            this.opponentCard.TabStop = false;
            // 
            // GameScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(1539, 635);
            this.Controls.Add(this.opponentCard);
            this.Controls.Add(this.oppScoreLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.forfeitButton);
            this.Controls.Add(this.playerScoreLabel);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GameScreen";
            this.Text = "Cards Game";
            this.Load += new System.EventHandler(this.GameScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.opponentCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label playerScoreLabel;
        private System.Windows.Forms.Button forfeitButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label oppScoreLabel;
        private System.Windows.Forms.PictureBox opponentCard;
    }
}

