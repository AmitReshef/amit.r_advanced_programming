﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace WarGame
{
    class ServerListener
    {
        private volatile bool _shouldStop;
        private NetworkStream socketStream;
        private Action<string> manageInput;
        private readonly int BUFFER_SIZE;

        public ServerListener(NetworkStream socketStream, Action<string> manageInput, int bufferSize = 4)
        {
            this.socketStream = socketStream;
            this.manageInput = manageInput;
            this.BUFFER_SIZE = bufferSize;
            _shouldStop = false;
        }

        public void Listen()
        {
            byte[] inBuffer = new byte[BUFFER_SIZE];
            string input;
            ASCIIEncoding enc = new ASCIIEncoding();
            int readValue;
            while (!_shouldStop)
            {
                if (socketStream.DataAvailable)
                {
                    readValue = socketStream.Read(inBuffer, 0, BUFFER_SIZE);
                    if (readValue != 0)
                    {
                        input = enc.GetString(inBuffer);
                        manageInput(input);
                    }
                }
            }
            Console.WriteLine("Worker terminated successfully..");
        }

        public void RequestStop()
        {
            Console.WriteLine("Worker termination requested..");
            _shouldStop = true;
        }

    }

    public partial class GameScreen : Form
    {
        private const int CARDS_NUM = 10;
        private const int PORT = 8820;
        private string opponentData;
        private string cardPicked;
        private int playerScore;
        private int oppScore;

        private TcpClient client;
        private IPEndPoint serverEndPoint;
        private ASCIIEncoding enc;
        private NetworkStream clientStream;

        private ServerListener worker;
        private Thread workerThread;

        private bool closeFlag;
        private bool gotClosingMessage;

        private SortedList<string, Bitmap> cards;
        private Button prevClicked;

        private void FreezeGUI()
        {
            foreach (Control c in this.Controls)
            {
                c.Enabled = false;
            }
        }
        private void UnFreezeGUI()
        {
            foreach (Control c in this.Controls)
            {
                c.Enabled = true;
            }
            GenerateCards();
        }

        private void GenerateCards()
        {
            Point pos = new Point(34, 256);
            Size size = new Size(90, 118);
            int marginRight = 96;
            for (int i = 0; i < CARDS_NUM; i++)
            {
                Button b = new Button();
                b.Click += CardClick;
                b.BackgroundImage = Properties.Resources.card_back_red;
                b.BackgroundImageLayout = ImageLayout.Stretch;
                b.Size = size;
                b.Location = pos;
                this.Controls.Add(b);
                pos.X += marginRight;
            }
        }

        private void manageInput(string input)
        {
            switch (input)
            {
                case "0000":
                    InitiateGame();
                    break;
                case "2000":
                    gotClosingMessage = true;
                    Invoke((MethodInvoker)(this.Close));
                    break;
                default:
                    GotCardCode(input);
                    break;
            }
        }

        private void InitiateGame()
        {
            Invoke((MethodInvoker)UnFreezeGUI);
        }

        private void SetScore()
        {
            playerScoreLabel.Text = playerScore.ToString();
            oppScoreLabel.Text = oppScore.ToString();
        }

        private void GotCardCode(string input)
        {
            opponentData = input.Substring(1);
            if (cardPicked != null)
            {
                opponentCard.BackgroundImage = cards[opponentData];
                int score = CompareCards(cardPicked, opponentData);
                playerScore += score;
                oppScore -= score;
                Invoke(((MethodInvoker)SetScore));
                opponentData = null;
                cardPicked = null;
            }
        }

        private int CompareCards(string cPlayer, string cOpponent)
        {
            int cP = Int32.Parse(cPlayer.Substring(0, 2));
            int cO = Int32.Parse(cOpponent.Substring(0, 2));
            if (cP > cO)
                return 1;
            else if (cP < cO)
                return -1;
            else
                return 0;
        }

        private void Game_Closing(object sender, CancelEventArgs e)
        {
            if (!closeFlag)
            {
                MessageBox.Show("Your score: " + playerScoreLabel.Text +
                            "\n Opponent's Score:" + oppScoreLabel.Text, "Game Results", MessageBoxButtons.OK);
            }

            if (!gotClosingMessage && !closeFlag)
            {
                byte[] outBuffer = enc.GetBytes("2000");
                clientStream.Write(outBuffer, 0, 4);
                clientStream.Flush();
            }

            if (workerThread != null && workerThread.IsAlive)
            {
                worker.RequestStop();
                if (!gotClosingMessage)
                    workerThread.Join();
            }

            if (client != null)
            {
                client.Close();
            }
        }

        private void Shown_Game(object snder, EventArgs e)
        {
            if (closeFlag)
            {
                this.Close();
            }
        }

        private void CardClick(object sender, EventArgs args)
        {
            if (cardPicked == null)
            {
                Random r = new Random();
                string pick = cards.Keys[r.Next(0, 52)];
                cardPicked = pick;
                Button b = sender as Button;
                b.BackgroundImage = cards[pick];
                b.Enabled = false;

                if (prevClicked != null)
                {
                    prevClicked.BackgroundImage = Properties.Resources.card_back_red;
                    prevClicked.Enabled = true;
                }

                prevClicked = b;

                byte[] outBuffer = enc.GetBytes("1" + pick);
                clientStream.Write(outBuffer, 0, 4);
                clientStream.Flush();

                if (opponentData != null)
                {
                    opponentCard.BackgroundImage = cards[opponentData];
                    int score = CompareCards(cardPicked, opponentData);
                    playerScore += score;
                    oppScore -= score;
                    Invoke(((MethodInvoker)SetScore));
                    cardPicked = null;
                    opponentData = null;
                }
                else
                {
                    opponentCard.BackgroundImage = Properties.Resources.card_back_blue;
                }
            }
        }

        private void forfeitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public GameScreen()
        {
            try
            {
                InitializeComponent();
                playerScore = 0;
                oppScore = 0;
                closeFlag = false;
                gotClosingMessage = false;
                cardPicked = null;
                prevClicked = null;

                cards = new SortedList<string, Bitmap>()
                {
                    { "02H",Properties.Resources._2_of_hearts },
                    { "02S",Properties.Resources._2_of_spades },
                    { "02C",Properties.Resources._2_of_clubs },
                    { "02D",Properties.Resources._2_of_diamonds },
                    { "03H",Properties.Resources._3_of_hearts },
                    { "03S",Properties.Resources._3_of_spades },
                    { "03C",Properties.Resources._3_of_clubs },
                    { "03D",Properties.Resources._3_of_diamonds },
                    { "04H",Properties.Resources._4_of_hearts },
                    { "04S",Properties.Resources._4_of_spades },
                    { "04C",Properties.Resources._4_of_clubs },
                    { "04D",Properties.Resources._4_of_diamonds },
                    { "05H",Properties.Resources._5_of_hearts },
                    { "05S",Properties.Resources._5_of_spades },
                    { "05C",Properties.Resources._5_of_clubs },
                    { "05D",Properties.Resources._5_of_diamonds },
                    { "06H",Properties.Resources._6_of_hearts },
                    { "06S",Properties.Resources._6_of_spades },
                    { "06C",Properties.Resources._6_of_clubs },
                    { "06D",Properties.Resources._6_of_diamonds },
                    { "07H",Properties.Resources._7_of_hearts },
                    { "07S",Properties.Resources._7_of_spades },
                    { "07C",Properties.Resources._7_of_clubs },
                    { "07D",Properties.Resources._7_of_diamonds },
                    { "08H",Properties.Resources._8_of_hearts },
                    { "08S",Properties.Resources._8_of_spades },
                    { "08C",Properties.Resources._8_of_clubs },
                    { "08D",Properties.Resources._8_of_diamonds },
                    { "09H",Properties.Resources._9_of_hearts },
                    { "09S",Properties.Resources._9_of_spades },
                    { "09C",Properties.Resources._9_of_clubs },
                    { "09D",Properties.Resources._9_of_diamonds },
                    { "10H",Properties.Resources._10_of_hearts },
                    { "10S",Properties.Resources._10_of_spades },
                    { "10C",Properties.Resources._10_of_clubs },
                    { "10D",Properties.Resources._10_of_diamonds },
                    { "11H",Properties.Resources.jack_of_hearts },
                    { "11S",Properties.Resources.jack_of_spades  },
                    { "11C",Properties.Resources.jack_of_clubs},
                    { "11D",Properties.Resources.jack_of_diamonds},
                    { "12H",Properties.Resources.queen_of_hearts },
                    { "12S",Properties.Resources.queen_of_spades  },
                    { "12C",Properties.Resources.queen_of_clubs},
                    { "12D",Properties.Resources.queen_of_diamonds },
                    { "13H",Properties.Resources.king_of_hearts },
                    { "13S",Properties.Resources.king_of_spades  },
                    { "13C",Properties.Resources.king_of_clubs},
                    { "13D",Properties.Resources.king_of_diamonds },
                    { "01H",Properties.Resources.ace_of_hearts },
                    { "01S",Properties.Resources.ace_of_spades  },
                    { "01C",Properties.Resources.ace_of_clubs},
                    { "01D",Properties.Resources.ace_of_diamonds }
                };

                this.FormClosing += Game_Closing;
                this.Shown += Shown_Game;

                client = new TcpClient();
                serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), PORT);
                enc = new ASCIIEncoding();
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();

                worker = new ServerListener(clientStream, manageInput);
                workerThread = new Thread(() =>
                {
                    try
                    {
                        worker.Listen();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Secondary process Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                });

                FreezeGUI();
                workerThread.Start();
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message, "Socket ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                closeFlag = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Socket ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                closeFlag = true;
            }
        }

        private void GameScreen_Load(object sender, EventArgs e)
        {

        }
    }
}