#include <windows.h>
#include <iostream>
#include <string>
#include <vector>
#include "Helper.h"

using namespace std;

typedef int(__cdecl *MYPROC)(void);

vector<string> parse_line()
{
	string buff;
	cout << ">>";
	getline(cin, buff);
	Helper::trim(buff);
	vector<string> commands = Helper::get_words(buff);
	return commands;
}

void print_failure(bool succeed, const string &msg)
{
	if (succeed)
	{
		return;
	}
	cout << msg << endl << "error:" << GetLastError() << endl;
}

LPSTR pwd()
{
	DWORD buffer_len = MAX_PATH;
	LPSTR buffer = new CHAR[buffer_len];
	GetCurrentDirectoryA(buffer_len, buffer);
	return buffer;
}

void cd(LPSTR dir)
{
	bool success = SetCurrentDirectoryA(dir);
	print_failure(success, "Failed to change dir");
}

void create(LPSTR file)
{
	HANDLE hFile = CreateFileA(file, GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	bool isCreated = !(INVALID_HANDLE_VALUE == hFile);
	if (isCreated)
	{
		CloseHandle(hFile);
	}
	print_failure(isCreated, "Failed to create file");


}

void ls(LPCSTR dir)
{
	_WIN32_FIND_DATAA findData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	hFind = FindFirstFileA(dir, &findData);

	bool success = (INVALID_HANDLE_VALUE != hFind);
	print_failure(success, "Failed to find files in directory");
	if (!success)
	{
		return;
	}


	do
	{
		cout << findData.cFileName << endl;
	} while (FindNextFileA(hFind, &findData) != 0);


	cout << endl;
	DWORD error = GetLastError();
	success = (ERROR_NO_MORE_FILES == error);
	print_failure(success, "Failed to find all files");

	FindClose(hFind);
}

void secret(LPSTR func)
{
	HINSTANCE hinstLib = LoadLibrary(TEXT("Secret.dll"));

	bool success = (NULL != hinstLib);
	print_failure(success, "Failed to load dll");
	if (!success)
	{
		return;
	}

	MYPROC procAdd = (MYPROC)GetProcAddress(hinstLib, func);

	success = (NULL != procAdd);
	print_failure(success, "Failed to load function");
	if (!success)
	{
		FreeLibrary(hinstLib);
		return;
	}

	cout << func << ":" << (procAdd)() << endl;
	FreeLibrary(hinstLib);
}

void execute(LPSTR executable)
{
	PROCESS_INFORMATION pi;
	STARTUPINFOA si;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	bool success = CreateProcessA(NULL, executable, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);

	print_failure(success, "Failed to create process");
	if (!success)
	{
		return;
	}

	WaitForSingleObject(pi.hProcess, INFINITE);
}


int main(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	vector<string> command = parse_line();
	while (command[0] != "exit")
	{
		if (command[0] == "pwd") // Print current dir.
		{
			cout << pwd() << endl;
		}
		else if (command[0] == "cd") // Change dir.
		{
			cd(const_cast<char*>(command[1].c_str()));

		}
		else if (command[0] == "create") // Create new file
		{
			create(const_cast<char*>(command[1].c_str()));
		}
		else if (command[0] == "ls") // List files in directory
		{
			string adr = command.size() < 2 ? "." : command[1];
			adr += "\\*";
			ls(adr.c_str());
		}
		else if (command[0] == "secret") // Find the secret in the dll
		{
			secret(const_cast<char*>("TheAnswerToLifeTheUniverseAndEverything"));
		}
		else if (command[0].find(".exe") == command[0].size() - 4) // Execute exe
		{
			execute(const_cast<char*>(command[0].c_str()));
		}


		command = parse_line();
	}
	return 0;
}

