#include "sqlite3.h"
#include <iostream>
#include <string>
#include <iomanip>
#include <locale>
#include <sstream>

using namespace std;

static int temp;

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	/*int i;

	for (i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
	}*/

	temp = stoi(argv[0]); // turning the string to an int
	//cout << endl << temp << endl;

	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg) 
{ 
	string str_buyerId;  
	ostringstream convert;      
	convert << buyerid;         
	str_buyerId = convert.str();
	int data_arry[3] = {}; //  [0] = is the buyer balance  [1] = is the car price  [2] is if the car is availble

	string str_carId;                                       
	convert << carid;          
	str_carId = convert.str(); 

	int rc;
	string str;

	string str_temp;

	str = "select * from cars where id = " + str_carId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	// getting the balance of the buyer
	str = "select balance from accounts where Buyer_id = " + str_buyerId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	data_arry[0] = temp;

	// getting the price of the car the buyer wants (according to the id of the car)
	str = "select price from cars where id = " + str_carId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	data_arry[1] = temp;

	// getting the availability of the car the buyer wants (according to the id of the car)
	str = "select available from cars where id = " + str_carId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	data_arry[2] = temp;



	if ((data_arry[0] >= data_arry[1]) && (data_arry[2] == 1))
	{
		// seting the balance of the buyer according to the car price
		temp = data_arry[0] - data_arry[1];
		cout << temp << endl;

		convert << temp;
		str_temp = convert.str();

		str = "update accounts set balance = " + str_temp + " where Buyer_id = " + str_buyerId;

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		// seting the availability of the car the buyer wanted
		str = "update cars set available = 0 where id = " + str_carId;

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		cout << "Purchase succeeded" << endl;

		return true;
	}
	else
	{
		cout << "Purchase faild" << endl;
		return false;
	}
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string str_fromId;
	ostringstream convert;
	convert << from;
	str_fromId = convert.str();
	int data_arry[2] = {}; //  [0] = balance of the person who passes the money  [1] = balance of the person thet gets the money

	string str_toId;
	convert << to;
	str_toId = convert.str();

	int rc;
	string str;

	string str_temp;

	// begin the transferation
	str = "begin transaction";

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}


	// getting the balance of the person who passes the money
	str = "select balance from accounts where Buyer_id = " + str_fromId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	data_arry[0] = temp;

	// getting the balance of the person thet gets the money
	str = "select balance from accounts where Buyer_id = " + str_toId;

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	data_arry[1] = temp;

	if (data_arry[0] >= amount) // meaning thet the persons balance is bigger then the amount he wants to transfer
	{
		temp = data_arry[0] - amount; 

		convert << temp;
		str_temp = convert.str();

		str = "update accounts set balance = " + str_temp + " where Buyer_id = " + str_fromId;

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}


		temp = data_arry[1] + amount;

		convert << temp;
		str_temp = convert.str();

		str = "update accounts set balance = " + str_temp + " where Buyer_id = " + str_toId;

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		cout << "Transfer succeeded" << endl;
		return true;
	}
	else
	{
		cout << "Transfer faild" << endl;
		return false;
	}
	

	// end the transferation
	str = "commit";

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	string str;
	
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	carPurchase(1, 3, db, zErrMsg);

	balanceTransfer(1, 2, 34000, db, zErrMsg);

	system("PAUSE");
	sqlite3_close(db);
}