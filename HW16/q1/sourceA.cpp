#include "sqlite3.h"
#include <iostream>
#include <string>

using namespace std;

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
	}

	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	string str;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	// creating the table
	str = "create table pepole(id integer primary key autoincrement, name string)";
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	// inserting first person
	str = "insert into pepole (name) values('A')";
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	// inserting second person
	str = "insert into pepole (name) values('B')";
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	// inserting third person
	str = "insert into pepole (name) values('C')";
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	// changing the name of the third person
	str = "update pepole set name = 'D' where name = 'C'";
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	// printing out the table
	str = "select * from pepole";
	rc = sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	system("PAUSE");
	sqlite3_close(db);
}