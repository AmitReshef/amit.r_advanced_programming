﻿using System;
using System.IO;
using System.Windows.Forms;

namespace login_wnd
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(checkUserNameAndPassword(UserNameTextBox.Text, PasswordTextBox.Text))
            {
                dates_wnd d = new dates_wnd();
                this.Hide();

                d.Username = UserNameTextBox.Text;

                MessageBox.Show("Welcome " + d.Username);

                d.ShowDialog();

                this.Show();
            }
            else
            {
                MessageBox.Show("The user name is wrong or it dosn't exist");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Good bye");
            Application.Exit();
        }

        private bool checkUserNameAndPassword(string UserName, string password)
        {
            bool canLogin = false;

            string usersTextFilePath = @"C:\Users\magshimim\Documents\Magshimim\Second_Year\CPP_Programing\Lesson19\Users.txt";

            foreach(string cust in File.ReadAllLines(usersTextFilePath))
            {
                if(cust == UserName + "," + password)
                {
                    canLogin = true;
                }
            }

            return canLogin;
        }
    }
}
