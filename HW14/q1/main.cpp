#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

class StringTree 
{
	private:
		class TreeNode 
		{
			friend class StringTree;
			string str;
			TreeNode *left;
			TreeNode *right;
			TreeNode(string s, TreeNode *l = NULL, TreeNode *r = NULL) 
			{
				str = s, left = l, right = r;
			}
		};
		TreeNode *root;

	   void displayInOrder(TreeNode *tree);
	   void displayPreOrder(TreeNode *tree);
	   void displayPostOrder(TreeNode *tree);

	   //prints using inorder method
	   void StringTree::displayInOrder(TreeNode *tree) 
	   {
			if (tree) 
			{
				displayInOrder(tree->left);
				cout << tree->str << " ";
				displayInOrder(tree->right);
			}
		}

		//prints using preorder method
		void StringTree::displayPreOrder(TreeNode *tree) 
		{
			if (tree) 
			{
				cout << tree->str << " ";
				displayPreOrder(tree->left);
				displayPreOrder(tree->right);
			}
		}

		//prints using post order method
		void StringTree::displayPostOrder(TreeNode *tree) 
		{
			if (tree) 
			{
				displayPostOrder(tree->left);
				displayPostOrder(tree->right);
				cout << tree->str << " ";
			}
		}

		//Class declaration for Binary seach tree
		class StringTree 
		{
			private:
				class TreeNode 
				{
					friend class StringTree;
					string str;
					TreeNode *left;
					TreeNode *right;
					TreeNode(string s, TreeNode *l = NULL, TreeNode *r = NULL) {
					str = s, left = l, right = r;
					}
				};
				TreeNode *root;

				void insert(TreeNode *&tree, string word);
				void remove(TreeNode *&tree, string word);
				void makeDeletion(TreeNode *&tree);
				void destroySubtree(TreeNode *tree);
				void displayInOrder(TreeNode *tree);
				void displayPreOrder(TreeNode *tree);
				void displayPostOrder(TreeNode *tree);
		

			public:
				//inline recursive functions
				StringTree()
				{
					root = NULL;
				}
				~StringTree()
				{
					destroySubtree(root);
				}

				bool search(string word);
				bool isBalanced(TreeNode *tree);
				int height(TreeNode *tree);

				void insert(string word)
				{
					insert(root, word);
				}

				void remove(string word)
				{
					remove(root, word);
				}

				void showInOrder(void)
				{
					displayInOrder(root);
				}

				void showPreOrder(void)
				{
					displayPreOrder(root);
				}

				void showPostOrder(void)
				{
					displayPostOrder(root);
				}
				void getTreeSize(int &size);
			};

			//for inserting into the tree
			void StringTree::insert(TreeNode *&tree, string word)
			{
				if (!tree)
				{
				tree = new TreeNode(word);
				return;
			}

			if (tree->str.compare(word))
				return;
			if (tree->str.compare(word)<0)
				insert(tree->left, word);
			else
				insert(tree->right, word);
		}

		//removing from the tree
		void StringTree::remove(TreeNode *&tree, string word)
		{
			if (!tree) return;
			if (tree->str.compare(word)<0)
				remove(tree->left, word);
			else if (tree->str.compare(word)>0)
				remove(tree->right, word);
			else
				makeDeletion(tree);
		}
		//searching for data in the tree
		bool StringTree::search(string word)
		{
			TreeNode *tree = root;
			while (tree)
			{
				if (tree->str.compare(word))
				{
					return true;
				}
				else if (tree->str.compare(word) < 0)
				{
					tree = tree->left;
				}
				else
				{
					tree = tree->right;
				}
			}
			return false;
		}
		//destroys a subtree
		void StringTree::destroySubtree(TreeNode *tree)
		{
			if (!tree) return;
			destroySubtree(tree->left);
			destroySubtree(tree->right);
			delete tree;
		}

		//prints using inorder method
		void StringTree::displayInOrder(TreeNode *tree) 
		{
			if (tree) {
				displayInOrder(tree->left);
				cout << tree->str << " ";
				displayInOrder(tree->right);
			}
		}

		//prints using preorder method
		void StringTree::displayPreOrder(TreeNode *tree) 
		{
			if (tree) 
			{
				cout << tree->str << " ";
				displayPreOrder(tree->left);
				displayPreOrder(tree->right);
			}
		}

		//prints using post order method
		void StringTree::displayPostOrder(TreeNode *tree) 
		{
			if (tree) 
			{
				displayPostOrder(tree->left);
				displayPostOrder(tree->right);
				cout << (tree->str) << " ";
			}
		}

		//for deleting
		void StringTree::makeDeletion(TreeNode *&tree) 
		{
			TreeNode *deleteNode;
			TreeNode *attach;

			if (!tree->right)
			{
				tree = tree->left;
			}
			else if (!tree->left)
			{
				tree = tree->right;
			}
			else
			{
				attach = tree->right;
			}

			while (attach->left)
			{
				attach = attach->left;
				attach->left = tree->left;
				tree = tree->right;
			}
			delete deleteNode;
		}

		void StringTree::getTreeSize(int &size)
		{
			TreeNode *tree = root;
			while (tree) 
			{
				if (tree->right)
				{
					size++;
					tree = tree->right;
				}
				if (tree->left)
				{
					size++;
					tree = tree->left;
				}
			}
		}//end getTreeSize

	bool StringTree::isBalanced(TreeNode *tree)
	{
		int lh; /* for height of left subtree */
		int rh; /* for height of right subtree */

				/* If tree is empty then return true */
		if (!tree)
		{
			return 1;
		}

		/* Get the height of left and right sub trees */
		lh = height(tree->left);
		rh = height(tree->right);

		if (abs(lh - rh) <= 1 && isBalanced(root->left) && isBalanced(root->right))
		{
			return 1;
		}

		/* If we reach here then tree is not balanced */
		return 0;
	}//end isBalanced

	int StringTree::height(TreeNode *tree)
	{
		if (!tree)
		{
			return 0;
		}

		else
		{
			/* compute the depth of each subtree */
			int lDepth = height(tree->left);
			int rDepth = height(tree->right);

			/* use the larger one */
			if (lDepth > rDepth)
			{
				return (lDepth + 1);
			}
			else 
			{
				return (rDepth + 1);
			}
		}
	} // end height
}