#ifndef _MYFILE_H
#define _MYFILE_H

#include <iostream>
#include <fstream>
#include <string>

using std::string;

class myFile
{
	public:

		static void printFile(string fileName);
		static void printWords(string inFile, string outFile);
};

#endif