#include <iostream>
#include <fstream>
#include <string>
#include "myFile.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

void main()
{
	string fileName = "example.txt";
	//myFile::printFile(fileName);

	string fin = "example.txt";
	string fout = "copy_file";

	myFile::printWords(fin, fout);

	system("pause");
}