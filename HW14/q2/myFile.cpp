#include <iostream>
#include <fstream>
#include <string>
#include "myFile.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;

void myFile::printFile(string fileName)
{
	string line;
	ifstream fin(fileName);

	while (getline(fin, line))
	{
		cout << line << '\n';
	}
	fin.close();
}

void myFile::printWords(string inFileName, string outFile)
{
	ifstream fin(inFileName);
	fin.open("data.txt", ios::in);

	ofstream fout(outFile);
	fout.open("new.txt", ios::out);

	char ch;
	char line[75];
	int i = 1;

	while (fin.get(ch))
	{
		fin.get(line, 75, '.');
		fout << "Line " << i << " : " << line << endl;
		i++;
	}

	fin.close();
}