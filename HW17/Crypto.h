#pragma once

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string.h>
#include "osrng.h"
#include "des.h"
#include "modes.h"

using namespace std;

class Crypto
{
public:
	Crypto();

	string encryptAES(string plainText);
	string decryptAES(string cipherText);
	char encryptRSA(int m, int e, int n);

private:
	string plainText, cipherText, decryptedText;
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};