#include "Crypto.h"

Crypto::Crypto() {}

string Crypto::encryptAES(string plainText) 
{

	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

string Crypto::decryptAES(string cipherText) 
{

	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	//  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

char Crypto::encryptRSA(int m, int e, int n)
{
	int res = 1;
	while (e > 0) 
	{
		if (e % 2 != 0) {
			res = (m*res) % n;
		}
		m = (m*m) % n;
		e /= 2;
	}
	return '0' + res;
}